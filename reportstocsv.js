const fs = require('fs');
const { Parser, transforms } = require('json2csv');
let errors = require('./errors_1.json');
let successes = require('./onboard_1.json');

async function convertToCSV(){  
  const errorParser = new Parser();
  const errorsCSV = errorParser.parse(errors);

  const successParser = new Parser();
  const successCSV = successParser.parse(successes);
  
  fs.writeFileSync('errors.csv', errorsCSV);
  fs.writeFileSync('success.csv', successCSV);
}

convertToCSV();