const JoiBase = require('joi');
const JoiDate = require('@hapi/joi-date');
const specialitys = require('./specializations.json');
const Joi = JoiBase.extend(JoiDate);

const languageSchema = Joi.string().valid(
  'English',
  'Kannada',
  'Hindi',
  'Tamil',
  'Telugu',
  'Marathi',
  'Bengali',
  'Malayalam',
);

const professionalExperienceSchema = Joi.object({
  role: Joi.string().required(),
  source: Joi.string().required(),
  city: Joi.string().allow(null, ''),
  from: Joi.date().iso().format('YYYY-MM-DDTHH:mm:ss.SSSZ').max(new Date())
    .required(),
  to: Joi.date().iso().format('YYYY-MM-DDTHH:mm:ss.SSSZ').min(Joi.ref('from')),
  is_current: Joi.boolean().strict(),
});

const educationSchema = Joi.object({
  degree: Joi.string().required(),
  source: Joi.string().required(),
  city: Joi.string().allow(null, ''),
  from: Joi.date().iso().format('YYYY-MM-DDTHH:mm:ss.SSSZ').max(new Date()),
  to: Joi.date().iso().format('YYYY-MM-DDTHH:mm:ss.SSSZ').min(Joi.ref('from')),
});

const membershipSchema = Joi.object({
  name: Joi.string().required(),
});

const registrationSchema = Joi.object({
  name: Joi.string().allow(null, ''),
  registrationNumber: Joi.string().required(),
  source: Joi.string().allow(null, ''),
  date: Joi.date().iso().format('YYYY-MM-DDTHH:mm:ss.SSSZ').max(new Date()),
  isVerified: Joi.boolean(),
});

const create = Joi.object().keys({
  "softLimitDuration": Joi.number().required(),
  "offlineDuration": Joi.number().required(),
  "onlineDuration": Joi.number().required(),
  "primarySpecializationId": Joi.string().valid(...specialitys.map(x => x._id)).required(),
  "gender": Joi.string().valid('M', 'F'),
  "department": Joi.string().required(),
  "about": Joi.string().required(),
  "licenseNo": Joi.string().required(),
  "expDate": Joi.date().iso().format('YYYY-MM-DDTHH:mm:ss.SSSZ').max(new Date()),
  "experience": Joi.number().required(),
  "additionalSpecializations": Joi.string().allow(null, ''),
  "specialization": Joi.string().required(),
  "qualification": Joi.string().required(),
  "phone": JoiBase.string().length(12).required(),
  "password": Joi.string().required(),
  "email": JoiBase.string().email().required(),
  "surname": Joi.string(),
  "name": Joi.string().required(),
  "title": Joi.string().required(),
  "languages": Joi.array().items(languageSchema).min(1).allow(null),
  "themeId": Joi.string(),
  // "languages": JoiBase.string(),
})

const createCSV = Joi.array().items(create);

module.exports = {
  createCSV,
  createPayload: create
};
