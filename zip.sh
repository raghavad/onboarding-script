#! /bin/bash
filename=`date "+%Y%m%d"`
echo "Zipping up all reports"
zip -r "${filename}.zip" data/*.csv
rm data/*.csv
rm data/*.json
rm csvdump.csv
echo "Zip created"