const csvtojson = require('csvtojson');
const { Parser } = require('json2csv');
const fs = require('fs');
const requestPromise = require('request-promise');
const validation = require('./onboardschema');
const headers = {
  "x-secret-key": "dEn3iQDYLnoZvXxbFw2dRIEXsg9v0B95ziKQgI8zEh6oQyaSX85siSFWsLM3C2vk",
  "x-authenticated-user": "59ca1fa66b97ca8212896514",
  'x-mclient-id': 'conf',
  'x-access-token': 'wrong-token',
  'Content-Type': 'application/json'
}

const transformToPayload = (obj) => {
  return {
    ...obj,
    isActive: true,
    "languages": obj.languages,
    "mfineRoleId": 2,
    "participantGroups": [],
    "availability": [],
    "introVideo": {
      "coverImage": "",
      "url": ""
    },
    "chatVideo": {
      "coverImage": "",
      "url": ""
    },
    "interactionConfig": {
      "ONLINE": {
        "duration": obj.onlineDuration,
        "showAs": "BUSY"
      },
      "OFFLINE": {
        "duration": obj.offlineDuration,
        "showAs": "BUSY"
      },
      "softLimit": obj.softLimitDuration
    },
    "participantGroups": [],
    "registrations": [
      {
        "registrationNumber": obj.licenseNo,
        "isVerified": true
      }
    ],
    "settingsPayload": {
      "online_slot_duration": obj.onlineDuration,
      "offline_slot_duration": obj.offlineDuration,
      "theme_id": obj.themeId
    }
  }
}

async function delay(ms) {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(true);
    }, ms);
  })
}

async function loadDoctorProfile(payload) {
  return requestPromise({
    method: 'POST',
    url: `https://prodc.mfine.co/api/userservice/users/onboardDoctor`,
    body: payload,
    json: true,
    headers
  })
}

async function updateSettings(id, payload) {
  return requestPromise({
    method: 'POST',
    url: `https://prodc.mfine.co/api/participantservice/Profiles/${id}/settings`,
    body: payload,
    json: true,
    headers
  })
}

async function processOnboard() {
  let jsonArray = await csvtojson().fromFile('./csvdump.csv');
  // const batchSize = jsonArray.length; // If you want to run in smaller batches than all at 1 shot, set this field.
  // const batchNumber = 0; // 0 - n;
  // if (batchNumber > Math.ceil(jsonArray.length / batchSize)) return true;
  // jsonArray = jsonArray.filter((x, i) => i < (batchNumber + 1) * batchSize && i >= batchNumber * batchSize);
  // jsonArray.length = 1;
  const id = new Date().getTime();
  const outfile = `data/onboard_doctors_${id}.json`;
  const errfile = `data/error_onboard_doctors_${id}.json`;
  fs.appendFileSync(outfile, '[{}', 'utf8');
  fs.appendFileSync(errfile, '[{}', 'utf8');
  await jsonArray.reduce(async (prev, next, index) => {
    await prev;
    // index += batchNumber * batchSize; // To maintain index consistency with csv
    await delay(100);
    console.log(`Processing index ${index}`);
    next.languages = next.languages.split();
    const { error } = validation.createPayload.validate(next, { abortEarly: false });
    if (error) {
      fs.appendFileSync(errfile, `,${JSON.stringify({ index: index, phone: next.phone, status: 'fail', error })}`, 'utf8');
      console.log(`Error in index - ${index}`);
      return true;
    }
    try {
      const payload = transformToPayload(next);
      const doctorProfile = await loadDoctorProfile(payload);
      await updateSettings(doctorProfile.id, { settings: payload.settingsPayload });
      fs.appendFileSync(outfile, `,${JSON.stringify({
        index: index, status: 'success',
        name: doctorProfile.name,
        surname: doctorProfile.surname,
        phone: next.phone,
        id: doctorProfile.id
      })}`, 'utf8');
      console.log(`Processed index ${index}`);
      return true;
    } catch (error) {
      console.log(`Error in index - ${index}`);
      fs.appendFileSync(errfile, `,${JSON.stringify({ index: index, phone: next.phone, status: 'fail', message: error.message })}`, 'utf8');
      return true;
    }
  }, true)

  fs.appendFileSync(outfile, ']', 'utf8');
  fs.appendFileSync(errfile, ']', 'utf8');

  // Generating CSV Reports
  const errors = require(`./${errfile}`);
  const successes = require(`./${outfile}`);
  const errorParser = new Parser();
  const errorsCSV = errorParser.parse(errors);

  const successParser = new Parser();
  const successCSV = successParser.parse(successes);

  fs.writeFileSync(`${errfile}.csv`, errorsCSV);
  fs.writeFileSync(`${outfile}.csv`, successCSV);

  // Done generating CSV Reports
  return true;
}

processOnboard()